package com.mikhaihurski.akor_test.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mikhaihurski.akor_test.databinding.RowBinding
import com.mikhaihurski.akor_test.dto.PhotoTypeDtoOut

class MyAdapter (private val onClickListener: OnClickListener) :
    ListAdapter<PhotoTypeDtoOut, MyAdapter.ViewHolder>(DiffCallback) {

    class ViewHolder(private var binding: RowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(dto: PhotoTypeDtoOut) {
            binding.dto = dto
            binding.executePendingBindings()
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<PhotoTypeDtoOut>() {
        override fun areItemsTheSame(oldItem: PhotoTypeDtoOut, newItem: PhotoTypeDtoOut): Boolean = oldItem === newItem
        override fun areContentsTheSame(oldItem: PhotoTypeDtoOut, newItem: PhotoTypeDtoOut): Boolean = oldItem.id == newItem.id
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(RowBinding.inflate(LayoutInflater.from(parent.context)))


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dto = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(dto)
        }
        holder.bind(dto)
    }


    class OnClickListener(val clickListener: (dto: PhotoTypeDtoOut) -> Unit) {
        fun onClick(dto: PhotoTypeDtoOut) = clickListener(dto)
    }
}
