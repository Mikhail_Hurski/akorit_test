package com.mikhaihurski.akor_test.di.binding

import android.annotation.SuppressLint
import android.util.Log
import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mikhaihurski.akor_test.R
import com.mikhaihurski.akor_test.adapter.MyAdapter
import com.mikhaihurski.akor_test.dto.PhotoTypeDtoOut


@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<PhotoTypeDtoOut>?) {
    val adapter = recyclerView.adapter as MyAdapter
    adapter.submitList(data)
}

@SuppressLint("ResourceType")
@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    Log.d("TAG", "bindImage: $imgUrl")
    if (imgUrl == null){
        Glide.with(imgView.context)
            .load(R.string.not_found)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image))
            .into(imgView)
    } else {
        var imgUri = imgUrl.toUri().buildUpon().scheme("https").build()
        Glide.with(imgView.context)
            .load(imgUri)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image))
            .into(imgView)
    }


}


