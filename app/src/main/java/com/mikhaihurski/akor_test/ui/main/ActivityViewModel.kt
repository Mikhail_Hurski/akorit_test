package com.mikhaihurski.akor_test.ui.main

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mikhaihurski.akor_test.dto.PhotoTypeDtoOut
import com.mikhaihurski.akor_test.network.RetrofitApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.lang.Exception
import kotlin.collections.List

enum class ApiStatus { LOADING, ERROR, DONE }

class ListViewModel : ViewModel() {

    private val mIsLoading = ObservableBoolean(false)



    private val _status = MutableLiveData<ApiStatus>()

    val status: LiveData<ApiStatus>
        get() = _status

    val dto: LiveData<List<PhotoTypeDtoOut>> get() = _dto
    private val _dto = MutableLiveData<List<PhotoTypeDtoOut>>()

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
        getMarsRealEstateProperties()
    }

    private fun getMarsRealEstateProperties() {
        coroutineScope.launch {
            var getListDto = RetrofitApi.retrofitApiService.getListDto(0)

            try {
                _status.value =
                    ApiStatus.LOADING
                val listResult = getListDto.await()
                _status.value =
                    ApiStatus.DONE
                setIsLoading(true)
                _dto.value = listResult.content as List<PhotoTypeDtoOut>?

            } catch (e: Exception) {
                _status.value =
                    ApiStatus.ERROR
                _dto.value = ArrayList()
            }
        }
    }

    fun getIsLoading(): ObservableBoolean = mIsLoading
    fun setIsLoading(isLoading: Boolean) =  mIsLoading.set(isLoading)

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}