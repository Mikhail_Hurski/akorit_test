package com.mikhaihurski.akor_test.ui.main

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.mikhaihurski.akor_test.R
import com.mikhaihurski.akor_test.adapter.MyAdapter
import com.mikhaihurski.akor_test.databinding.ActivityMainBinding
import com.mikhaihurski.akor_test.ui.fragment.PresenterPerson
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val viewModel: ListViewModel by lazy {
        ViewModelProviders.of(this).get(ListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding =
            DataBindingUtil.setContentView<ActivityMainBinding>(this,
                R.layout.activity_main
            )
        binding.lifecycleOwner = this
        binding.vm = viewModel
        binding.rvMachine.adapter = MyAdapter(MyAdapter.OnClickListener {
            rvMachine.visibility = View.GONE
            supportFragmentManager.beginTransaction()
                .add(R.id.frame,PresenterPerson(it))
                .addToBackStack("a")
                .commit()
        })

    }

    override fun onBackPressed() {
        super.onBackPressed()
        rvMachine.visibility = View.VISIBLE

    }
}