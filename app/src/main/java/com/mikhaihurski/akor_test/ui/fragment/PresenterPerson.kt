package com.mikhaihurski.akor_test.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.mikhaihurski.akor_test.R
import com.mikhaihurski.akor_test.databinding.PresenterPersonFragmentBinding
import com.mikhaihurski.akor_test.dto.PhotoTypeDtoOut

class PresenterPerson(var person:PhotoTypeDtoOut) : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = PresenterPersonFragmentBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.tvID.text = "ID: ${person.id}"
        binding.tvName.text = "Name : ${person.name}"
        Glide.with(this)
            .load(person.image)
            .error(R.drawable.ic_broken_image)
            .into(binding.ivImage)
        return binding.root
    }

}