package com.mikhaihurski.akor_test.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.mikhaihurski.akor_test.dto.PageDto
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

private const val BASE_URL = "https://junior.balinasoft.com/"
private val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()



object RetrofitApi {
    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()

    val retrofitApiService: RetrofitApiService by lazy {
        retrofit.create(RetrofitApiService::class.java)
    }
}

interface RetrofitApiService {

    @GET("api/v2/photo/type")
    fun getListDto(@Query("page") numberPage:Int): Deferred<PageDto>

}