package com.mikhaihurski.akor_test.dto

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class PhotoTypeDtoOut(
    var id: Int? = null,
    var name: String? = null,
    var image: String? = null
) : Parcelable