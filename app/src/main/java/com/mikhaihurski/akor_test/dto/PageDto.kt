package com.mikhaihurski.akor_test.dto

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class PageDto (
    var content: List<PhotoTypeDtoOut?>? = null,
    var page: Int? = null,
    var pageSize: Int? = null,
    var totalElements: Int? = null,
    var totalPages: Int? = null
):Parcelable