package com.mikhaihurski.akor_test.dto

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class MachineTools(

    var id: Int,
    var productId:String,
    var type: String,
    var typeurl:String,
    var model: String,
    var modelurl:String,
    var producer: String,
    var producingCountry: String,
    var systemCNC:String,
    var fullSystemCNC:String,
    var year1:Int,
    var condition1: String,
    var machineLocation: String,
    var axisCount:String,
    var diameterMax: Int,
    var diameter: Int,
    var bardiameter:Int,
    var lengthMax: Int,
    var x: Int,
    var y: Int,
    var z: Int,
    var spindleSpeed:Int,
    var spindlepower:Int,
    var spindlediameter:Int,
    var subspindle:Int,
    var subspindleSpeed:Int,
    var vdi:Int,
    var toolsall:Int,
    var toolslive:Int,
    var toolsnotlive:Int,
    var tailstock:Int,
    var accuracy:Int,
    var accuracyAxisC:Int,
    var spindletime:Int,
    var machineRuntime:Int,
    var price:Int,
    var descriptionen:String,
    var descriptionru:String,
    var video1: String,
    var video2:String,
    var manufacturer:String,
    var year:Int,
    var country:String,
    var photos: Array<Photo>,
    var exist: String

) : Parcelable

@Parcelize
class Photo(
    var id: Int,
    var photo: String
) : Parcelable